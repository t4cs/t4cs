import logging
import os
import os.path

from collections import OrderedDict
from pathlib import Path

import AssnUtils as utils
import AssnTasks

# This is an example of a much more complex assignment. We originally
#  used this as a midterm exam question, although it turned out to
#  probably be too long for exam use.

# This assignment is designed to walk a student through writing a
#  simple script that handles copying or linking files, with the added
#  twist that all files are hidden files (. prefixed).

# This assignment has 4 sequential parts:
#   a starter reminder
#   a simple 'ls' filtering task
#   a file-copying task
#   a file-linking task


# On more complex assignments we asked students to always prefix
#  errors their scripts emit, that way we can filter our errors they
#  intentionally emit vs bugs in their script.
ERR_PREFIX = "[ dotscript error ]"


# For larger assignments, we write our descriptions at the top of the
#  class, to make it easy to quickly edit problem descriptions.
#  Generally we found that we made edits to the problem description
#  rather that to the code of the problem.  All of the implementation
#  code is after the descriptions

# This task is a reminder to students that they will need to use an
#  'ls' option flag to see hidden files. We usually made these worth 0
#  points, but block progress to the rest of the assignment.
LS_GATE_DESCRIPTION = """

Recall that "ls" doesn't show all files by default.

What flag to "ls" will allow it to show files beginning with a "."?

Omit the '-' character in your answer.
Ex: if the answer were -j, answer 'j'
"""

# This task requires the student to write and submit a bash script
#  that filters ls results for dot-prefixed files.
DOTLISTER_DESCRIPTION = """

In this problem you will write a script that identifies dotfiles.
You will want to write a loop for this script.

You script should:
  + Take 1 argument, which is a path to a directory (dir_path)
  + Print out just the filename of every dotfile in that directory,
    one per line WITHOUT the dot. (See example)
    (You will want to ignore the special files "." and "..")
  + Print in alphabetic order (this is the default for ls)

Your output should:
  + Not print any output to stderr
  + Not print the leading dot on filenames
  + Not print any filenames that were not dotfiles

You can assume:
  + The input path exists and is a directory
  + There are no directories starting with a .
  + All dotfiles have a name of at least 2 characters
    (ie.  ".xx" is a possibility, but ".x" is not)
  + All dotfiles have a maximum of 1 dot in their name.
    (ie. ".xx" is a possibility, but ".x.x" is not)


Hints:
  + A solution to this problem can consist of a loop, with a single
    if statement inside.
  + The conditional check can be written several different ways, choose
    one that feels comfortable to you
  + Try testing your solution with a directory that has dotfiles, and one
    that does not have any dotfiles
===================================================================

Example:

  $ ls -a /example/data/
    .  ..  .abc  .grades  foobar  magic  test_questions  .zeta

  $ ./dotscript.sh /example/data/
    abc
    grades
    zeta

"""

# This task expands the previous script-writing task to copy the
#  dot-prefixed files. Note that it also requires the script to make a
#  directory and handle error cases.
DOTSCRIPT_DESCRIPTION = """

In this problem you will write a script that copies dotfiles. You
should expand the previous script you wrote.

You script should:
  + Take 1 argument, which is a path to a directory (dir_path)
  + Make a new directory in dir_path called dotfiles if one does not exist
  + Emit an error if the dotfiles directory exists and
    immediately exit with non-0 status
  + Copy every dotfile in dir_path into the dotfiles directory without
    the leading dot (again, ignore . and ..)


Your output should:
  + Not print any output to stdout
  + Only print to stderr with the prefix "[ dotscript error ]"
  + Only print to stderr if the dotfiles directory exists already


You can assume:
  + The input path exists and is a directory
  + If the dotfiles directory did NOT exist, you will not overwrite any files
  + All dotfiles have a name of at least 2 characters
    (ie.  ".xx" is a possibility, but ".x" is not)

Hints:
  + You will want to be sure to test the case of the dotfiles directory
    already existing.
  + You should not need to add more than one additional conditional
    check to your previous script.

===================================================================

Example:

  $ ls -a /example/data/
    .  ..  .abc  .grades  foobar  magic  test_questions  .zeta

  $ ./dotscript.sh /example/data/

  $ ls /example/data/dotfiles/
   abc  grades  zeta


Error Example:

  $ ls -a /example/data/
   .  ..  .abc  .grades  dotfiles  magic  test_questions  .zeta

  $ ./dotscript.sh /example/data/
    [ dotscript error ] dotfiles dir already exists!

  $ echo $?
    1

"""

# Finally, the last task requires them to sub in the use of symlinks
#  rather than copy the files directly. Asking the students to do this
#  task first we found to be too much, so it has several parts that
#  lead up to it.
LNDOTSCRIPT_DESCRIPTION = """

In this problem you will modify the previous script to link the
dotfiles rather than copy.

You script will:
  + Satisfy all previous requirements except copying
  + Will NOT copy the dotfiles
  + Will make a symbolic link rather than copy
  + The symbolic links should be absolute paths

Your script should:
  + Not print any output to stdout
  + Only print to stderr with the prefix "[ dotscript error ]"
  + Only print to stderr if the dotfiles directory exists already

"""


# Internal utility checker that checks if symlinks are setup as
#  requested. Can return multiple error messages depending on what
#  goes wrong to hint to the student what went wrong in their
#  script. We try to balance helpful messages with giving away too
#  much of the exact checks being performed. We don't want to allow
#  students to quickly reverse engineer the tests, we want them to
#  follow the spec.
def _checker_lndotscript(cwd, dest, files):
    # Easy way to check if links are abs path
    import shutil
    shutil.move(os.path.join(dest, "dotfiles/"),
                os.path.join(cwd, "dotfiles/"))

    for f in files:
        link = Path(cwd).joinpath("dotfiles/").joinpath(f)
        if not link.is_symlink():
            return False, "Missing symlink"
        if not link.exists():
            return False, "Link not absolute"
        if not link.resolve() == Path(dest).joinpath("." + f):
            return False, "Link points to wrong file"

    return True, None


# Simple utility to look for errors emitted by the submitted script
#  that are not supposed to occur. If its prefixed, the student
#  intended to emit it.
def _stderr_prefix_checker(lines):
    if not lines:
        return False

    for l in lines.split("\n"):
        if not l.startswith(ERR_PREFIX):
            return False

    return True


# We use decorators for extremely common tasks, like ensuring that the
#  submitted script starts with #!/bin/bash as this one does.

# For submitted scripts, we define a submission handler that has a
#  series of tests they will run against the submission. The order is
#  meaningful, as the checker will exit on the first failed test.
@utils.require_bashbang
def _submit_dotlister(assignment, filename):

    # Tests are defined as an array of dictionaries, where the defined
    #  keys in a given test indicate how to run the test.

    # Test names never appear to students, so they are generally
    #  verbose to aid instructors in debugging.
    DOTLISTER_TESTS = [

        # This test will pass the given src_path to the input
        #  script, check the exit code, ensure stdout and stderr are empty.
        # Note that all of these are callable. See the test harness code
        #  for more details. Description is the error the student will see.

        {
            "name": "should_succeed_nofiles",
            "src_path": "dotscript/ok/in1",
            "exit_code_check": lambda x: x == 0,
            "stdout_check": lambda x: not x,
            "stderr_check": lambda x: not x,
            "args": lambda x: [os.path.join(x, "")],
            "description": "no files handling (desired exit status: 0)",
        },

        # Very similar to the above test, except that it expects some
        #  amount of output from the test script.

        {
            "name": "should_succeed_files",
            "src_path": "dotscript/ok/in2",
            "exit_code_check": lambda x: x == 0,
            "stdout_check": lambda x: x == "file_copy\nseeme\ntest\n",
            "stderr_check": lambda x: not x,
            "args": lambda x: [os.path.join(x, "")],
            "description": "basic files handling (desired exit status: 0)",
        },
    ]

    # Test harness running code, note that it handles creating the
    # src_path here!
    for test in DOTLISTER_TESTS:
        if "src_path" in test:
            test['src_path'] = os.path.join(
                utils.get_assignment_src_dir(assignment), test['src_path'])
        # Run the actual test harness
        success, errmsg = utils.run_test(filename, test)
        if not success:
            errmsg = "Failed in testing {}: {}".format(
                test['description'], errmsg)
            assignment.error(errmsg)
            return False
    return True


# This is very similar to the previous task. However, the tasks now
#  include a 'ref_path' which specifies what the directory should look
#  like after the script runs. This is very useful for file system
#  manipulation scripts since you can just create a starting point
#  (src_path) and an ending point (ref_path) and the harness will
#  handle making sure the script transformed the file system correctly.
@utils.require_bashbang
def _submit_dotscript(assignment, filename):

    DOTSCRIPT_TESTS = [
        {
            "name": "should_succeed_files",
            "src_path": "dotscript/ok/in2",
            "ref_path": "dotscript/ok/out2",
            "exit_code_check": lambda x: x == 0,
            "stdout_check": lambda x: not x,
            "stderr_check": lambda x: not x,
            "args": lambda x: [os.path.join(x, "")],
            "description": "basic files handling (desired exit status: 0)",
        },
        {
            "name": "should_error_dir_exists",
            "src_path": "dotscript/bad/in1",
            "ref_path": "dotscript/bad/out1",
            "exit_code_check": lambda x: x != 0,
            "stdout_check": lambda x: not x,
            "stderr_check": lambda x: _stderr_prefix_checker,
            "args": lambda x: [os.path.join(x, "")],
            "description": "basic error handling (desired exit status: non-0)",
        },

    ]
    # Note that we construct both src and ref path here!
    for test in DOTSCRIPT_TESTS:
        if "src_path" in test:
            test['src_path'] = os.path.join(
                utils.get_assignment_src_dir(assignment), test['src_path'])
        if "ref_path" in test:
            test['ref_path'] = os.path.join(
                utils.get_assignment_src_dir(assignment), test['ref_path'])

        success, errmsg = utils.run_test(filename, test)
        if not success:
            _logger.info(
                "Submission of %s failed in %s with: %s",
                filename, test['name'], errmsg)
            errmsg = "Failed in testing {}: {}".format(
                test['description'], errmsg)
            assignment.error(errmsg)
            return False
    return True


# Again, very similar to the previous tasks, now just using that
#  _checker_lndotscript to examine symlinks.
@utils.require_bashbang
def _submit_lndotscript(assignment, filename):

    LNDOTSCRIPT_TESTS = [
        {
            "name": "should_succeed_files",
            "src_path": "dotscript/ok/in2",
            "exit_code_check": lambda x: x == 0,
            "stdout_check": lambda x: not x,
            "stderr_check": lambda x: not x,
            "args": lambda x: [os.path.join(x, "")],
            "final_cb": lambda a, b: _checker_lndotscript(
                a, b, ["file_copy", "seeme", "test"]),
            "description": "basic files handling (desired exit status: 0)",
        },
        {
            "name": "should_error_dir_exists",
            "src_path": "dotscript/bad/in1",
            "ref_path": "dotscript/bad/out1",
            "exit_code_check": lambda x: x != 0,
            "stdout_check": lambda x: not x,
            "stderr_check": lambda x: _stderr_prefix_checker,
            "args": lambda x: [os.path.join(x, "")],
            "description": "basic error handling (desired exit status: non-0)",
        },

    ]

    for test in LNDOTSCRIPT_TESTS:
        if "src_path" in test:
            test['src_path'] = os.path.join(
                utils.get_assignment_src_dir(assignment), test['src_path'])
        if "ref_path" in test:
            test['ref_path'] = os.path.join(
                utils.get_assignment_src_dir(assignment), test['ref_path'])

        success, errmsg = utils.run_test(filename, test)
        if not success:
            _logger.info(
                "Submission of %s failed in %s with: %s",
                filename, test['name'], errmsg)
            errmsg = "Failed in testing {}: {}".format(
                test['description'], errmsg)
            assignment.error(errmsg)
            return False
    return True


# Finally, we construct the list of tasks in an OrderedDict, since
#  there are now hard dependencies that stop a student from jumping
#  ahead. Additionally, tasks have an explicit point value now (visible
#  to the student).
ASSIGNMENT_TASKS = OrderedDict([
    # This task is an answer task, meaning it expects a string from
    #  the student, not a script, and it will not run the checker at
    #  all times.
    ("ls_reminder", {
        "name": "ls flag reminder",
        "description": LS_GATE_DESCRIPTION,
        "dependencies": [],
        "answer": lambda asn, a: (a == "-a" or a == "a"),
        "points": 1,
    }),
    # This task and future tasks take a script, and so are _submit_
    #  tasks. Again, they don't run any code unless the student
    #  submits something using the class tools.
    ("dot_lister", {
        "name": "dotfiles lister",
        "description": DOTLISTER_DESCRIPTION,
        "dependencies": ["ls_reminder"],
        "submit": _submit_dotlister,
        "points": 3,
    }),
    ("dot_script", {
        "name": "dotfiles copying script",
        "description": DOTSCRIPT_DESCRIPTION,
        "dependencies": ["dot_lister"],
        "submit": _submit_dotscript,
        "points": 5,
    }),
    ("dotln_script", {
        "name": "dotfiles linking script TODO",
        "description": LNDOTSCRIPT_DESCRIPTION,
        "dependencies": ["dot_script"],
        "submit": _submit_lndotscript,
        "points": 2,
    }),
])

# To make it easier on students, we have a summary description that
#  appears in overviews.
class Assignment(AssnTasks.Assignment):
    tasks = ASSIGNMENT_TASKS
    description = "Write scripts to copy dotfiles."
