FROM ubuntu:22.04
LABEL version="1.0"

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN apt-get install make -y
RUN apt-get install gcc -y
RUN apt-get install git -y
RUN apt-get install sudo -y
RUN apt-get install build-essential -y
RUN apt-get install python3 -y
RUN apt-get install python3-pip -y
RUN apt-get install ttyrec -y
RUN apt-get install openssh-server -y
RUN apt-get install tmux -y
RUN apt-get install emacs-nox -y
RUN apt-get install vim -y
RUN apt-get install mosh -y
RUN pip3 install termcolor

RUN git config --global user.name "t4cs"
RUN git config --global user.email "dockertest@t4cs"


RUN mkdir -p /run/sshd && chmod 755 /run/sshd


EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]

COPY . /t4cs-setup
WORKDIR /t4cs-setup/private

RUN echo "t4cs-student1" > students
RUN echo "t4cs-student2" >> students
RUN echo "t4cs-instructor1" > instructors
RUN echo "t4cs-instructor2" >> instructors

RUN ./setup_instructors.sh

RUN echo "Match User *\n\tForceCommand /bin/bash -c \"/var/t4cs/bin/ssh_forced_command\"\n" >> /etc/ssh/sshd_config
RUN usermod -aG sudo t4cs-instructor1
RUN usermod -aG sudo t4cs-instructor2

# Let t4cs setup handle the rest. Run this image with -p 2222:22 or similar and ssh in
RUN ./install.sh test
