#!/bin/bash

set -e

STUDENTS_FILE=students
INSTRUCTORS_FILE=instructors

T4CS_REQ_ROOT=/var/t4cs
CURRENT_DIR=$(dirname $(readlink -f $0))
T4CS_CUR_ROOT=$(dirname $CURRENT_DIR)

T4CS_ROOT=${T4CS_REQ_ROOT}

TEST_MODE=0

# Check if we're in testing (docker) mode
if [[ $# -gt 0 ]] && [[ $1 == "test" ]]; then
    TEST_MODE=1
    printf "IN TEST MODE. We'll assume stuff. Use for docker build testing/etc.\n"
fi

# A group containing only course students
readonly STUDENT_GROUP=t4cs_students
# A group used for slightly elevated permissions for logging, etc.
readonly SUBMIT_GROUP=t4cs_submit
# A group containing only course admins
readonly INSTRUCTOR_GROUP=t4cs_admins

# If we aren't being run from the right place, complain
if [[ $CURRENT_DIR != $(pwd) ]]; then
    printf "\n[Error] Please run install.sh from where t4cs is located, ie: ./install.sh\n"
    exit 1
fi

# Ask to install prereqs
if [[ $TEST_MODE -eq 1 ]];then
    INSTALL_PREREQ='n'
else
    printf "We can try to auto-install pre-requisites (apt-get + pip), install? [y/N] "
    read INSTALL_PREREQ
fi

if [[ $INSTALL_PREREQ == "y" ]] || [[ $INSTALL_PREREQ == "Y" ]]; then
    printf "Attempting install, you may need to fix this\n"
    apt-get update
    apt-get install python3 python3-pip ttyrec
    pip3 install termcolor
fi

# Check if there already is a /var/t4cs
if [[ -e $T4CS_REQ_ROOT ]]; then
    printf "t4cs is already installed to ${T4CS_REQ_ROOT}, do you want to continue setup? [Y/n] "
    read INSTALL
    if [[ $INSTALL == "n" ]] || [[ $INSTALL == "N" ]]; then
	exit 1
    fi
else
    # Setup the root link
    ln -s ${T4CS_CUR_ROOT} ${T4CS_REQ_ROOT}
fi

# TODO Ask for a course name
#printf "What is the class called? [T4CS] "
PROPER_NAME=T4CS
#read PROPER_NAME

printf "What is the shortname (no spaces)? [t4cs] "
SHORT_NAME=t4cs
#read SHORT_NAME


# Setup groups that are needed, these must match what is in adduser.sh
if [[ $(getent group ${STUDENT_GROUP}) ]]; then
    printf "Group ${STUDENT_GROUP} exists, skipping creation...\n"
else
    addgroup ${STUDENT_GROUP}
fi
if [[ $(getent group ${SUBMIT_GROUP}) ]]; then
    printf "Group ${SUBMIT_GROUP} exists, skipping creation...\n"
else
    addgroup ${SUBMIT_GROUP}
fi
if [[ $(getent group ${INSTRUCTOR_GROUP}) ]]; then
    printf "Group ${INSTRUCTOR_GROUP} exists, skipping creation...\n"
else
    addgroup ${INSTRUCTOR_GROUP}
fi


# Ensure only the instructors group can read various important dirs
DANGEROUS_DIRS="${T4CS_CUR_ROOT}/private"

chgrp $SUBMIT_GROUP ${T4CS_CUR_ROOT}
chmod o-rw ${T4CS_CUR_ROOT}

chgrp $SUBMIT_GROUP ${T4CS_CUR_ROOT}/assignments
chmod o-rw ${T4CS_CUR_ROOT}/assignments

for dir in "$DANGEROUS_DIRS"; do
    chgrp $INSTRUCTOR_GROUP $dir
    chmod o-rwx $dir
done



# Create home directories for students
if [[ -e ${T4CS_ROOT}/home ]]; then
    printf "${T4CS_ROOT}/home already exists, skipping to adding users...\n"
else
    mkdir ${T4CS_ROOT}/home
fi

# Create all the student accounts
if [[ ! -e ${STUDENTS_FILE} ]]; then
    printf "[Error] No students file found, please create ${STUDENTS_FILE}\n"
    exit 1
fi

USR_PW_LOCAL=${T4CS_REQ_ROOT}/private/student_usernames_passwords
printf "Creating student accounts, emiting a copy of all usernames/passwords to ${USR_PW_LOCAL}\n"
for STUDENT in $(cat ${STUDENTS_FILE}); do
    # ignore user if they exist, why did that happen?
    if [[ ! $(id -u ${STUDENT} 2>&1 > /dev/null) ]]; then
	echo "=========================================================================" | tee -a ${USR_PW_LOCAL}
	printf "User ${STUDENT} already exists, skipping...\n" | tee -a ${USR_PW_LOCAL}
    else
	${T4CS_ROOT}/environment/adduser/adduser.sh $STUDENT | tee -a ${USR_PW_LOCAL}
    fi
done

# Add admins to the instructor group
# We don't create users for this group, they need to already exist
if [[ ! -e ${INSTRUCTORS_FILE} ]]; then
    printf "[Warning] No Instructors file found, there will be no instructors!\n"
else
    for INSTRUCTOR in $(cat ${INSTRUCTORS_FILE}); do
	if [[ $(id -u ${INSTRUCTOR} 2>&1 > /dev/null) ]]; then
	    printf "INSTRUCTOR user ${INSTRUCTOR} doesn't exist! Skipping adding them to instructor group...\n"
	    printf "You can add them to the ${INSTRUCTOR_GROUP} group manually after you create the user.\n"
	else
	    adduser \
		--quiet \
		$INSTRUCTOR $INSTRUCTOR_GROUP > /dev/null
	    adduser \
		--quiet \
		$INSTRUCTOR $SUBMIT_GROUP > /dev/null
	    printf "${INSTRUCTOR} added to instructors+submit group\n"
	fi
    done
fi

# Generate the sgid wrapper
T4CS_ROOT=$T4CS_REQ_ROOT
cd ${T4CS_REQ_ROOT}/autograder
T4CS_ROOT=$T4CS_REQ_ROOT T4CS_SUBMIT_GROUP=$SUBMIT_GROUP make
if [[ ! $? ]]; then
    printf "[Error] Building the wrapper for the course commands failed, try building it manually with the autograder/Makefile\n"
fi

printf "\n\nSetup is complete!\nIf you need to add/remove users later refer to SETUP\n"
printf "**** A copy of all student usernames + pws is in ${USR_PW_LOCAL} ****\n"
