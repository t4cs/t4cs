#!/bin/bash

readonly ESC="\e"

readonly BOLD=1
readonly DIM=2
readonly UNDERLINE=4
readonly INVERSE=7

readonly FG_BLACK=30
readonly FG_RED=31
readonly FG_GREEN=32
readonly FG_YELLOW=33
readonly FG_BLUE=34
readonly FG_MAGENTA=35
readonly FG_CYAN=36
readonly FG_LGRAY=37
readonly FG_DGRAY=90
readonly FG_LRED=91
readonly FG_LGREEN=92
readonly FG_LYELLOW=93
readonly FG_LBLUE=94
readonly FG_LMAGENTA=95
readonly FG_LCYAN=96
readonly FG_LWHITE=97

readonly BG_BLACK=40
readonly BG_RED=41
readonly BG_GREEN=42
readonly BG_YELLOW=43
readonly BG_BLUE=44
readonly BG_MAGENTA=45
readonly BG_CYAN=46
readonly BG_LGRAY=47
readonly BG_DGRAY=100
readonly BG_LRED=101
readonly BG_LGREEN=102
readonly BG_LYELLOW=103
readonly BG_LBLUE=104
readonly BG_LMAGENTA=105
readonly BG_LCYAN=106
readonly BG_LWHITE=107

readonly RESET=${ESC}[0m

function color() {
    echo -en "$ESC["
    for c in $@; do
        [[ "$nf" ]] && echo -en ';'
        echo -en "$c"
        nf=1
    done
    echo -en "m"
}

function color_reset() {
    echo -en $RESET
}
