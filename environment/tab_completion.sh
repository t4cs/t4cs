_wants_assignment() {
    local cur=${COMP_WORDS[COMP_CWORD]}
    local assignments=$($T4CS_TASK_BIN available --plain $@)
    COMPREPLY=( $(compgen -W "$assignments" -- $cur) )
}

_wants_task() {
    local cur=${COMP_WORDS[COMP_CWORD]}
    local tasks=$($T4CS_TASK_BIN info $1 --plain)
    COMPREPLY=( $(compgen -W "$tasks" -- $cur) )
}

_wants_filename() {
    _filedir
}

_t4cs_task()
{
    local index=$COMP_CWORD
    local cur=${COMP_WORDS[COMP_CWORD]}
    local prev=${COMP_WORDS[COMP_CWORD-1]}

    actions="available activate info answer next submit"

    if [[ $index -eq 1 ]]; then # need an action
        COMPREPLY=( $(compgen -W "$actions" -- $cur) )
        return 0
    fi

    # Dispatch to each subcommand:
    case "${COMP_WORDS[1]}" in
    available)
        return 0
        ;;
    activate)
        _wants_assignment --all
        return 0
        ;;
    answer)
        [[ "$index" -eq 2 ]] && _wants_assignment && return 0
        [[ "$index" -eq 3 ]] && _wants_task $prev && return 0
        return 0
        ;;
    next)
        [[ "$index" -eq 2 ]] && _wants_assignment && return 0
        return 0
        ;;
    info)
        [[ "$index" -eq 2 ]] && _wants_assignment && return 0
        return 0
        ;;
    submit)
        [[ "$index" -eq 2 ]] && _wants_assignment && return 0
        [[ "$index" -eq 3 ]] && _wants_task $prev && return 0
        [[ "$index" -eq 4 ]] && _wants_filename   && return 0
        return 0
        ;;
    *)
        # Unknown command
        return 1
    esac

}

complete -F _t4cs_task t4cs_task
complete -F _t4cs_task $T4CS_TASK_BIN
