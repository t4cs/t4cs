#!/bin/bash

# Where is T4CS installed?
readonly INSTALL_DIR=/var/t4cs

# A group containing only course students
readonly STUDENT_GROUP=t4cs_students

# A group used for slightly elevated permissions for logging, etc.
readonly SUBMIT_GROUP=t4cs_submit

# A group containing only course admins
readonly INSTRUCTOR_GROUP=t4cs_admins

# Where is the skeleton directory?
readonly SKELETON_DIR=${INSTALL_DIR}/environment/adduser/skel

######## No editing required below this line ########

readonly HOME_ROOT=$INSTALL_DIR/home


readonly NUM_ARGS=1
set -e
usage() {
    if [[ "$@" ]]; then
        echo -e "ERROR: $@\n" >&2
    fi

    echo "Usage: $0 username" >&2
    exit 1
}

if [[ "$#" -ne $NUM_ARGS ]]; then
    usage "Invalid Arguments"
fi

username=$1

if [[ $(whoami) != 'root' ]]; then
    usage "Only root may create users."
fi

home_dir=$HOME_ROOT/$username

# Copy skeleton files -- we can do this directly with adduser, but then we'd
# need a custom configuration, which is hard to parameterize.
cp -r $SKELETON_DIR $home_dir

password=$(mktemp -u XXXXXXXX)
adduser \
    --quiet \
    --home $home_dir \
    --disabled-login \
    --gecos '' \
    $username
adduser \
    --quiet \
    $username $STUDENT_GROUP > /dev/null
chpasswd <<< $username:$password

# Setup permissions -- we can do this directly with adduser, but then we'd
# need a custom configuration, which is hard to parameterize.
chown -R $username:$username $home_dir
chmod 750 $home_dir

# First create their logs dir
mkdir $INSTALL_DIR/logs/$username

# This is the directory that holds all of their ttyrecordings
mkdir $INSTALL_DIR/logs/$username/ttyrec
chgrp $INSTRUCTOR_GROUP $INSTALL_DIR/logs/$username/ttyrec
chmod 753 $INSTALL_DIR/logs/$username/ttyrec

# This is the logfile for command outputs. We create the file to protect against
# other users touching this file first...
touch $INSTALL_DIR/logs/$username/cmds.log
chown $username $INSTALL_DIR/logs/$username/cmds.log
chgrp $INSTRUCTOR_GROUP $INSTALL_DIR/logs/$username/cmds.log
chmod 240 $INSTALL_DIR/logs/$username/cmds.log

# Logfiles for assignment state and grades
mkdir $INSTALL_DIR/logs/$username/assns

mkdir -m 530 $INSTALL_DIR/logs/$username/assns/grades
chgrp $SUBMIT_GROUP $INSTALL_DIR/logs/$username/assns/grades

mkdir -m 530 $INSTALL_DIR/logs/$username/assns/state
chgrp $SUBMIT_GROUP $INSTALL_DIR/logs/$username/assns/state

touch $INSTALL_DIR/logs/$username/assns/internal
chgrp $SUBMIT_GROUP $INSTALL_DIR/logs/$username/assns/internal

echo "========================================================================="
echo "User: $username"
echo "Pass: $password"
