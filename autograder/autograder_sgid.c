
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int execv(const char *path, char *const argv[]);

int main(int argc, char ** argv) {
    setregid(getegid(), getegid());
    execv(SCRIPT, argv);
}
