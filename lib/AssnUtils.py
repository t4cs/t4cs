import itertools
import json
import logging
import os
import pwd
import sys
import tempfile
import textwrap
import termcolor
import tarfile

import AssnState


COURSE_NAME = os.environ["T4CS_PROPER_NAME"]
ROOT = os.environ["T4CS_ROOT"]
PREFIX = os.environ["T4CS_SHORT_NAME"]

# What command do they use to call this program?
EXE_NAME = PREFIX + "_task"

# This directory is relative to the user's home directory
USER_CONFIG_PATH = '.{}.conf'.format(PREFIX)

# What do we call the directory for state information in their home dirs?
USER_ASSIGNMENT_DIR_NAME = ".{}_assignment_data".format(PREFIX)

LOGS_BASE_PATH = os.path.join(ROOT, "logs")
GLOBAL_CONFIG_PATH = os.path.join(ROOT, "autograder/config.json")
ASSIGNMENTS_BASE_PATH = os.path.join(ROOT, "assignments")

HR_CHAR = "="
HR_WIDTH = 80
HR_COLOR = "blue"

SURGE_CUTOFFS = [  # (s, n): s in seconds, n is max number of OK requests
    (5, 15),        # 10 requests in  5 seconds
    (30, 30),       # 20 requests in 30 seconds
    (1800, 50),     # 30 requests in 30 minutes
]

BASH = "/bin/bash"

# This should be set to the arguments to the script as soon as we know them.
COMMAND_ARGS = None
GLOBAL_CONFIG = None
USER_CONFIG = None

_logger = logging.getLogger("util")


_first_print = True


class Assignment(object):
    """Base assignment.

    It should only have stuff that's relevant to ALL assignments.
    """
    # User-friendly and short name uniquely identifying the assignment.
    #   e.g. 009_forif
    name = None

    # To contain the AssnState.State object.
    state = None

    def __init__(self, name=None):
        if name:
            self.name = name
        state_fname = os.path.join(get_user_statedir(), name)

        try:
            self.state = AssnState.State(state_fname)
        except AssnState.StateException:
            fatal("[AssnState] Could not access state file {}!".format(
                state_fname))

    def __del__(self):
        if not self.state.save_and_close():
            fatal("[AssnState] Could not save state!")

    def finalize(self):
        """Called right before exit."""
        pass

    def is_complete(self):
        """Is this assignment complete?"""
        return self.state.is_completed()

    def announce(self, msg, msgcolor='cyan', save=True):
        """ Prints a message to the user, colorized and with a header.
        """

        display_announcement(msg, name=self.name, msgcolor=msgcolor)

        if save:
            self.state.set('last_announce', msg)

    def error(self, msg, msgcolor='yellow', save=True):
        """ Prints a message to the user, colorized and with a header.
        """
        display_error(msg, self.name, msgcolor)

        if save:
            self.state.set('last_announce', msg)

    def _file_exists_and_is_executable(self, filename):
        """Used by decorators for submission checks."""
        # TODO -- I HATE that this needs to be in the assignment, but it uses
        #         'error', and 'error' uses state. :-/
        if not os.path.exists(filename):
            self.error("Script doesn't exist.")
            return False

        # We check explicitly because we pipe through bash, and bash
        # will execute damn near anything.
        if not os.stat(filename).st_mode & 0o100:
            self.error("Script isn't executable.")
            return False

        return True

    def mark_complete(self):
        self.announce(
            "Assignment {} complete!".format(self.name), 'green')
        self.state.set_completed(True)

    def error_gated(self, msg):
        if self.state.get('last_announce') == msg:
            return False
        self.error(msg)
        return True

    def announce_gated(self, msg):
        if self.state.get('last_announce') == msg:
            return False
        self.announce(msg)
        return True

    # Assign a grade to the assignment if active
    def assign_grade(self, score, possible):
        # We don't assign a grade if not active, but say so
        if self.name not in GLOBAL_CONFIG.get('active_assignments', []):
            if self.announce_gated(
                    "Grade updates are not enabled for this assignment."):
                _logger.info(
                    "Refused to update grade for non-active assignment")
            return

        # TODO -- score / possible may  stored in the assignment
        path = os.path.join(get_user_gradesdir(), self.name)
        try:
            with open(path, 'w') as f:
                f.write(json.dumps({'score': score, 'possible': possible}))
        except IOError as e:
            fatal("[AssnUtils] Unable to write grade file! err:" + str(e))


def display_announcement(msg, name=None, msgcolor='cyan'):
    _logger.info("announced: %s...", msg.split("\n", 1)[0])
    display_banner(msg, "{} Announcement".format(COURSE_NAME), name,
                   headercolor='magenta', msgcolor=msgcolor)


def display_error(msg, name=None, msgcolor='yellow'):
    _logger.info("error'd: %s", msg)
    display_banner(msg, "{} Error".format(COURSE_NAME), name,
                   headercolor='red', msgcolor=msgcolor)


def display_hint(msg, name=None, msgcolor='cyan'):
    _logger.info("hinted: %s", msg)
    display_banner(msg, "{} Hint".format(COURSE_NAME), name=name,
                   headercolor='green', msgcolor=msgcolor)


def display_banner(msg, header=None, name=None, headercolor='magenta',
                   namecolor='yellow', msgcolor='yellow'):

    # Print a horizontal rule if it's the first banner after a 'check'.
    global _first_print
    if (COMMAND_ARGS.action == 'check' and _first_print and
            not COMMAND_ARGS.pipeline[-1][0].startswith(EXE_NAME)):
        print(colored(HR_CHAR * HR_WIDTH, HR_COLOR))
        _first_print = False

    print()

    print(
        colored("[{}]".format(header), headercolor,
                attrs=['bold', 'underline']) if header else "",
        colored(name, 'yellow', attrs=['bold']) if name else "")
    print(nice_indent(colored(msg, msgcolor)))


class SafeRunner(object):
    """Runs given script in a temporary directory.

    Forces everything through bash without the privileged flag, so will
    drop euid and egid.

    Sets a bogus working directory to try and break relative paths.

    By default, pushes stdin/out/err to dedicated file objects available
    at self.process.std{out,err,in}.  By default, line buffers the
    std* pipes.

    So long as you make sure that script is a file, I think we're ok.

    SafeRunner.process is the subprocess Popen object to communicate with:

        sr = SafeRunner("bad_script.sh")
        try:
            rv = sr.process.wait(timeout=5)
        except TimeoutExpired:
            sr.process.kill()

    Please note: if you use PIPE for the std*s, you need to make sure
    you empty them after you kill the process.

    .cwd, .home and .other include various useable directories.

    prep_cb is an optional callback which is passed the home, cwd and
    other directories prior to running.

    """
    _devnull = None

    def __init__(self, script, args=[], env={}, bufsize=1,
                 stdout=None,
                 stdin=None,
                 stderr=None,
                 prep_cb=None,
                 extra_path=None,
                 **kwargs):
        import subprocess

        script = os.path.abspath(script)
        self.cwd = tempfile.mkdtemp()
        self.home = tempfile.mkdtemp()

        if stdout is None or stderr is None or stdin is None:
            self._devnull = open("/dev/null", 'r+')
        if stdout is None:
            stdout = self._devnull
        if stderr is None:
            stderr = self._devnull
        if stdin is None:
            stdin = self._devnull

        path = os.environ["PATH"]
        if extra_path is not None:
            path = extra_path + ":" + path

        script = [BASH, script] + args
        realenv = {
            "HOME": self.home,
            "PATH": path,
            "USER": os.environ['USER'],
            "T4CS_ROOT": os.environ['T4CS_ROOT'],
        }
        if 'T4CS_TEST' in os.environ:
            realenv["T4CS_TEST"] = os.environ['T4CS_TEST']

        if prep_cb:
            prep_cb(self.home, self.cwd)

        # Keep this after prep_cb, in case the callback changes it
        realenv.update(env)

        self.process = subprocess.Popen(
            script, env=realenv, cwd=self.cwd, executable=BASH,
            stdout=stdout, stderr=stderr, stdin=stdin,
            **kwargs)

    def close(self):
        try:
            self.process.kill()
        except ProcessLookupError:
            pass

        import shutil

        shutil.rmtree(self.cwd)
        shutil.rmtree(self.home)

        if self._devnull:
            self._devnull.close()

        self.cwd = None

    def __del__(self):
        if self.cwd:
            self.close()


def _get_path_differences(path_a, path_b, name, file_compare_cb=None):
    """Used in run_test for user-friendly errors about path differences."""
    matched, diff = paths_match(path_a, path_b)
    if not matched:
        real_err = True
        if diff.left_only:
            errmsg = "files missing"
            _logger.info("Files missing from %s: %s", name, diff.left_only)
        if diff.right_only:
            errmsg = "extra files found"
            _logger.info("Extra files in %s: %s", name, diff.right_only)
        if diff.diff_files:
            # TODO: This isn't great, but it'll have to do
            if file_compare_cb is not None:
                real_err = False  # maybe!
                for fname in diff.diff_files:
                    if not file_compare_cb(
                            os.path.join(path_a, fname),
                            os.path.join(path_b, fname)):
                        errmsg = "contents of files differed"
                        _logger.info("Different files (w/checker) in %s: %s",
                                     name, diff.diff_files)
                        real_err = True
                        break
            else:
                errmsg = "contents of files differed"
                _logger.info("Different files in %s: %s",
                             name, diff.diff_files)
        if diff.funny_files:
            errmsg = "not all files were comparable"
            _logger.info("Funny files in %s: %s", name, diff.funny_files)
        if real_err:
            return (False,
                    "output not as expected in {} ({})".format(name, errmsg))

    return (True, None)


def run_test(filename, test):  # pylint: disable=too-many-branches
    import shutil
    import subprocess

    _logger.info("Running test: %s", test.get('name', "UNKNOWN"))
    arg_dir = tempfile.mkdtemp()

    # Slightly horrifying workaround.
    def cleanup(ret_val):
        if not test.get("keep_args_dir", False):
            shutil.rmtree(arg_dir)
        else:
            _logger.info("Didn't delete: %s", arg_dir)
        return ret_val

    dst_path = os.path.join(arg_dir, "test_directory")

    if 'src_path' in test:
        shutil.copytree(test["src_path"], dst_path)

    args = test.get("args", [])
    try:
        args = args(dst_path)
    except TypeError:
        pass

    sr = SafeRunner(
        filename,
        args=args,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stdin=subprocess.PIPE if "stdin" in test else None,
        extra_path=test.get("extra_path", None),
        prep_cb=test.get("prep_cb", None),
        env=test.get("extra_env", {}))
    try:
        (stdout, stderr) = sr.process.communicate(
            test["stdin"].encode() if "stdin" in test else None,
            timeout=test.get("timeout", 2))

        stdout = stdout.decode()
        stderr = stderr.decode()
        if stdout:
            _logger.info("First line of stdout: %s",
                         stdout.lstrip().split("\n", 1)[0][:120]
                         if stdout else None)
        if stderr:
            _logger.info("First line of stderr: %s",
                         stderr.lstrip().split("\n", 1)[0][:120]
                         if stderr else None)

    except subprocess.TimeoutExpired:
        sr.process.kill()
        return cleanup((False, "timed out"))

    if not test.get("stderr_check", lambda x: True)(stderr):
        return cleanup((False, "produced invalid stderr."))

    if not test.get("stdout_check", lambda x: True)(stdout):
        return cleanup((False, "produced invalid stdout."))

    if not test.get("cwd_status_check", lambda x: True)(sr.cwd):
        return cleanup((False, "failed working directory status check."))

    if not test.get("exit_code_check", lambda x: True)(
            sr.process.returncode):
        return cleanup(
            (False,
             "incorrect exit status ($?={}).".format(
                 sr.process.returncode)))

    if "ref_path_use_cwd" in test:
        success, msg = _get_path_differences(
            test['ref_path_use_cwd'], sr.cwd, "working directory",
            test.get('file_compare_cb', None))
        if not success:
            return cleanup((success, msg))

    if "ref_path" in test:
        success, msg = _get_path_differences(
            test['ref_path'], dst_path, "destination",
            test.get('file_compare_cb', None))
        if not success:
            return cleanup((success, msg))

    if "final_cb" in test:
        success, msg = test["final_cb"](sr.cwd, dst_path)
        if not success:
            return cleanup((success, msg))

    return cleanup((True, None))


def initialize(args):
    global COMMAND_ARGS
    COMMAND_ARGS = args
    read_global_config()
    read_user_config()


def read_global_config():
    global GLOBAL_CONFIG
    try:
        with open(GLOBAL_CONFIG_PATH, 'r') as f:
            config = json.load(f)
    except (IOError, ValueError) as e:
        # TODO this will keep firing until we get a config!
        _logger.error("Failed to load or parse global config file: " + str(e))
        fatal("Unable to parse global configuration file.")

    config['all_assignments'] = (
        config.get('active_assignments', []) +
        config.get('inactive_assignments', []))
    GLOBAL_CONFIG = config
    return config


def read_user_config():
    global USER_CONFIG
    config_path = os.path.join(get_user_homedir(), USER_CONFIG_PATH)
    if not os.path.exists(config_path):
        _logger.info("User has no local configuration file. Using blank.")
        USER_CONFIG = {}
        return

    try:
        with open(config_path, 'r') as f:
            USER_CONFIG = json.load(f)
    except (IOError, ValueError) as e:
        _logger.error(
            "User has corrupted local configuration file. Using blank! err:{}".
            format(str(e)))
        USER_CONFIG = {}


def save_user_config():
    global USER_CONFIG
    config_path = os.path.join(os.environ['HOME'], USER_CONFIG_PATH)
    with open(config_path, 'w') as f:
        json.dump(USER_CONFIG, f)


def colored(text, color=None, on_color=None, attrs=None, force=False):
    """Same as termcolor.colored, but only if stdout is a tty."""
    if force or sys.stdout.isatty() or os.environ.get('FORCE_COLOR', False):
        return termcolor.colored(text, color, on_color, attrs)
    return text


def nice_indent(txt, prefix="  ", width=None, mult=1):
    prefix = prefix * mult

    # I don't recommend setting width (and enabling wrapping)
    # when there are colors-- ascii control sequences count towards the width.
    bits = txt.split("\n")
    if width:
        tw = textwrap.TextWrapper(width=width - len(prefix))
        bits = itertools.chain.from_iterable(
            tw.fill(y).split("\n") for y in bits)

    return "\n".join([prefix + x for x in bits])


def fatal(msg="No error specified"):
    # Using stdout b/c we redirect stderr to /dev/null
    sys.stdout.write("{}{}\n".format(
        colored('[{} Error]\n'.format(COURSE_NAME),
                'red', attrs=['bold', 'underline']), msg))
    _logger.critical(msg)
    exit(-1)


def command_arg_prefix(prefix):
    args = set(COMMAND_ARGS.arguments)
    for a in args:
        if a.startswith(prefix):
            return True
    return False


def command_has_args(required, expand_flags=True):
    """Returns whether all in required were present in args.

    required     : a list of argsets to check to see if present.
    expand_flags : whether to expand multiple flags starting with a single dash
                   into multiple flags. e.g. '-xvz' -> ('-x', '-v', '-z')

    Returns True iff all of the args are present.

    An 'argset' here is either a string, or an iterable. If it's a string, we
    check for that string. If it's an iterable, we require at least one of
    those iterables.

    In this case, args is considered to be order-insensitive.

    """

    args = COMMAND_ARGS.arguments
    if expand_flags:
        arg_p = []
        for arg in args:
            if arg.startswith("-") and not arg.startswith("--"):
                for subarg in arg[1:]:
                    arg_p.append('-' + subarg)
            else:
                arg_p.append(arg)
        args = arg_p

    args = set(args)
    for requirement in required:
        if isinstance(requirement, str):
            requirement = (requirement,)

        found = False
        for option in requirement:
            if option in args:
                found = True
                break
        if not found:
            return False
    return True


def cmd_looks_like(cmd, args=None, force_zrv=True, expand_flags=True):
    """Returns whether cmd and args are as expected.

    In general, this isn't a great choice if ordering is hugely important. This
    makes NO promises regarding what order the arguments appeared in.

    cmd          : command (program) to expect.
    args         : A list of args to expect (e.g. ['-x', '-y', 'foo', 'bar'])
    force_zrv    : Fail if the return value was non-zero.
    expand_flags : Whether to expand multiple flags starting with a single dash
                   into multiple flags. e.g. '-xvz' -> ('-x', '-v', '-z')

    In this case, args is considered to be order-insensitive.

    """

    if COMMAND_ARGS.command != cmd:
        return False
    if force_zrv and COMMAND_ARGS.rv != 0:
        return False
    if args:
        return command_has_args(args, expand_flags=expand_flags)
    return True


def get_assn_name(fname):
    elements = fname.split("/")
    if elements[-1] != "main.py":
        fatal("get_assn_name: given non main.py filename:" + fname)
    return elements[-2]


def find_files(fname, start_dir='.'):
    results = []
    for root, _dirs, files in os.walk(start_dir):
        if fname in files:
            results.append(os.path.join(root, fname))
    return results


def get_user():
    return pwd.getpwuid(os.geteuid()).pw_name


def get_user_homedir():
    # TODO - better to just use $HOME?
    return pwd.getpwnam(get_user()).pw_dir + "/"


def get_user_assndir(assignment):
    return os.path.join(get_user_homedir(), assignment.name)


def get_user_gradesdir():
    return os.path.join(LOGS_BASE_PATH, get_user(), "assns/grades")


def get_user_statedir():
    return os.path.join(LOGS_BASE_PATH, get_user(), "assns/state")


def get_assignment_src_dir(assignment):
    return os.path.join(ASSIGNMENTS_BASE_PATH, assignment.name)


def tarfiles_superficial_match(tf1, tf2, force_compression_match=False):
    """Compares the contents of two tarfiles by names and paths only.

    Compression match will ensure that the compression mode used
    is the same for both files"""
    try:
        flag = "*"  # Default to not caring about compression
        if force_compression_match:
            if ".tgz" in tf1 or ".tar.gz" in tf1:
                flag = "gz"
            elif "tar.xz" in tf1:
                flag = "xz"
            elif "tar.bz2" in tf1:
                flag = "bz2"
        with tarfile.open(tf1, 'r:' + flag) as tfile:
            try:
                with tarfile.open(tf2, 'r:' + flag) as rfile:
                    return set(tfile.getnames()) == set(rfile.getnames())
            except (IOError, tarfile.ReadError):
                return False
    except tarfile.ReadError:
        return False
    return False


def paths_match(dir1, dir2):
    """Compare two directory trees' contents.

    Return False if they differ, True is they are the same.
    """

    import filecmp

    class dircmp(filecmp.dircmp):
        """
        Compare the content of dir1 and dir2. In contrast with filecmp.dircmp,
        this subclass compares the content of files with the same path.

        This trick blatantly stolen and modified from from:
        https://stackoverflow.com/questions/4187564/recursive-dircmp-compare
            -two-directories-to-ensure-they-have-the-same-files-and
        """
        def phase3(self):
            """Ensure we are using content comparison with shallow=False."""
            fcomp = filecmp.cmpfiles(self.left, self.right, self.common_files,
                                     shallow=False)

            # pylint: disable=W0201
            self.same_files, self.diff_files, self.funny_files = (
                fcomp
            )

    compared = dircmp(dir1, dir2)
    if (compared.left_only or
            compared.right_only or
            compared.diff_files or
            compared.funny_files):
        return False, compared
    for subdir in compared.common_dirs:
        submatch, subdesc = paths_match(
            os.path.join(dir1, subdir), os.path.join(dir2, subdir))
        if not submatch:
            return False, subdesc
    return True, None


def get_user_hidden_assndatadir(assignment):
    """Path of the current user's current assignment's hidden data directory.

    Located in their homedir under a hidden directory."""
    return os.path.join(get_user_homedir(),
                        USER_ASSIGNMENT_DIR_NAME,
                        assignment.name)


def get_assn_setupdir(assignment):
    return os.path.join(ROOT, "assignments", assignment.name)


def mkdir_p(path):
    """Mimics functionality of "mkdir -p" """
    try:
        os.makedirs(path)
    except OSError as e:
        import errno
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def require_bashbang(function):
    """Decorator that requires bash as a hashbang in an executable."""
    # TODO -- I HATE that this needs to be in the assignment, but it uses
    #         state implicitly.

    def wrapper(asn, filename):
        if not asn._file_exists_and_is_executable(filename):
            return False

        with open(filename, 'r') as f:
            if not f.readline().startswith("#!/bin/bash"):
                asn.error("Script is missing bash hashbang.")
                return False

        return function(asn, filename)
    return wrapper


def require_existing_executable_decorator(function):
    """A decorator for submit checkers.

    Ensures that file is an existing executable."""

    def wrapper(asn, filename):
        if not asn._file_exists_and_is_executable(filename):
            return False
        return function(asn, filename)
    return wrapper


def init_copy_untar_hidden(f, assignment):
    """Extracts a given tar gzip file into the user's hidden directory.

    Expects f to be the name of a .tgz file in pwd."""
    import tarfile
    try:
        with tarfile.open(f + ".tgz", 'r:gz') as tfile:
            tfile.extractall(path=get_user_hidden_assndatadir(assignment))
    except (tarfile.TarError, IOError) as e:
        fatal("Tried to setup with untaring. err: " + str(e))


def file_contains_strings(fname, strs):
    if type(strs) == str:
        strs = [strs]
    try:
        with open(fname, 'r') as f:
            data = f.read()
            for string in strs:
                if string not in data:
                    return False
    except IOError:
        return False
    return True


def call_with_asn_or_string(obj, asn):
    try:
        return obj(asn)
    except TypeError:
        return obj


def send_slack_notification(msg):
    slack = GLOBAL_CONFIG.get('slack', None)
    if not slack:
        _logger.error("No slack information available")
        return

    import urllib.request

    values = {
        "username": slack.get('username', 'Autograder'),
        "text": msg,
        "channel": slack['channel'],
        "icon_emoji": slack.get('emoji', ":warning:"),
        "mrkdwn": True,
    }

    url = slack['token_url']

    data = json.dumps(values).encode()
    req = urllib.request.Request(url, data)
    response = urllib.request.urlopen(req)
    response.read()


def check_surge_windows(assn, surge_type, checks=SURGE_CUTOFFS):
    """Returns number of seconds to wait if they're above the cutoffs."""
    surge_key = "surge:" + surge_type
    max_timeout = max(checks)[0]
    assn.state.log_event(surge_key, max_timeout)
    for (window, cutoff) in checks:
        if assn.state.n_events_in_last(surge_key, window) > cutoff:
            return window
    return False


def split(string, delims, quote_chars=None):
    """Split a string by the given delimiters.

    Respects the given set of quote characters, or escaped delimiters.
    (although, escaping only affects the next character).

    Delims should be a set, if you don't care about ordering, or a list if the
    ordering/precedence is important.

    WARNING: If you provide delims as a set, this function considers them in
        decreasing order of length. That way, if you have a delimiter that is a
        substring of another, you won't end up with two splits when you wanted
        one.

        If you provide your own ordering, either ensure that longer strings
        come first OR accept the consequences, wherein "a || b" split with
        ["|", "||"] will return, e.g., [("a ", "|"), ("", "|"), (" b", None)].

    Returns a list of (substring, delimiter) tuples. The final delimiter will
    be None, representing the end of the string.
    """
    if not quote_chars:
        quote_chars = ['"', "'"]

    quote_char = None
    escaped = False

    start = 0
    parts = []

    if type(delims) == set:
        delims = sorted(delims, key=len, reverse=True)

    it = enumerate(string)
    for i, char in it:
        if escaped:
            escaped = False
        elif char == "\\":
            escaped = True
        elif quote_char:
            if char == quote_char:
                quote_char = None
        elif char in quote_chars:
            quote_char = char
        else:
            for delim in delims:
                if string[i:i + len(delim)] == delim:
                    parts.append((string[start:i], delim))
                    start = i + len(delim)

                    # For delimiters more than a character in length, skip over
                    # the extra characters in the delimiter
                    to_skip = len(delim) - 1
                    for i in range(to_skip):
                        next(it)

                    break

    # Implicit split from last delimiter to the end
    parts.append((string[start:], None))

    return parts


def generic_pipeline_answer(assignment, task_name, answer, answers):
    if answer != '-':
        assignment.error("This question only accepts answers to stdin.")
        return False

    if assignment.is_task_complete(task_name):
        display_hint("This task has already been solved!")

    inp = sys.stdin.read()
    for check, result, msg in answers:
        try:
            if not check(inp, assignment):
                continue
        except Exception as e:
            _logger.exception("Exception on pipeline checker")
            continue

        if msg:
            display_hint(msg)

        if result:
            assignment.state.set(
                task_name + ':tmp_flag_answered_correctly', True)
            return (False, False)
        return (False, False)

    return False


def generic_pipeline_check(
        assignment, task_name, answers, start_file=None,
        min_pipeline_len=3, max_pipeline_len=None):

    def bail(msg, **kwargs):
        display_hint(msg, **kwargs)
        return False

    # If we're not even supposed to be checking for this task, bail early.
    if not assignment.state.get(task_name + ':tmp_flag_answered_correctly'):
        return False

    # Otherwise, clear the flag no matter what
    assignment.state.set(task_name + ':tmp_flag_answered_correctly', None)

    args = COMMAND_ARGS
    pipeline = args.pipeline

    # This is what the final command inthe pipeline should look like (aka us)
    final_command_target = "{} answer {} {} -".format(
        EXE_NAME, assignment.name, task_name)

    # Enforce the final command
    if not pipeline[-1][0] == final_command_target:
        return bail(
            "'{} answer' MUST be the last command in the pipeline.".format(
                EXE_NAME))

    # Enforce the initial command
    if start_file:
        if not pipeline[0][0] == "cat {}".format(start_file) or (
                pipeline[0][1] != '|'):
            return bail(
                "'cat {}' MUST be the first command in the pipeline.".format(
                    start_file))

    if min_pipeline_len and len(pipeline) < min_pipeline_len:
        return bail("That pipeline is too short to be a valid answer.")

    if max_pipeline_len and len(pipeline) > max_pipeline_len:
        return bail("That pipeline is too long to be a valid answer.")

    # Enforce that >, < or >> don't show up in the pipeline anywhere.
    # TODO -- This is a hack. It will fail if you have these operators in a
    #         string (e.g. sed 's/<//')
    BAD_SEPS = [">>", "<>", ">", "<"]
    for cmd, sep in pipeline:
        for bad in BAD_SEPS:
            if bad in cmd:
                return bail(
                    ("That pipeline contains a forbidden output operator"
                     " ({})").format(bad))

    # Lazy check -- this only works if you don't have a loop
    # for cmd, sep in pipeline[1:]:
    #     if (cmd.startswith("echo") or
    #             cmd.startswith("printf") or
    #             cmd.startswith("cat")):
    #         return bail("No answer injection, please")

    return True
