#!/usr/bin/python3
import os
import sys
import json
from datetime import datetime
import utmp

T4CS_ROOT = os.environ.get("T4CS_ROOT", "/var/t4cs")
sys.path.append(os.path.join(T4CS_ROOT, "lib"))
import AssnState  # noqa - need the import path first

# list of usernames to exclude from the listings
EXCLUDED_USERS = [
]


PATH_TO_CONFIG = "autograder/config.json"


class Grades(object):

    point_values = {}
    users = []
    days = []
    months = []
    assignments = {}
    attendance = {}

    def __init__(self, point_values={},
                 users=[], assignment_list=[], days=[], months=[]):
        self.point_values = point_values
        self.users = users
        self.days = days
        self.months = months
        for assn in assignment_list:
            self.assignments[assn] = self._get_all_user_grades(assn)
        self._get_all_attendance()

    def _get_all_user_grades(self, assn):
        grades = {}
        for user in self.users:
            state_fname = os.path.join(T4CS_ROOT, "logs/assns/state/", user,
                                       assn)
            grade_fname = os.path.join(T4CS_ROOT, "logs/assns/grades/", user,
                                       assn)
            try:
                state = AssnState.State(state_fname)
            except AssnState.StateException:
                grades[user] = 0
                continue
            cur = 0
            total = 0
            try:
                with open(grade_fname, 'r') as f:
                    gr = json.load(f)
                    cur = float(gr.get('score', 0))
                    total = float(gr.get('possible', 0))
                    if assn not in self.point_values.keys():
                        self.point_values[assn] = total

                    grades[user] = cur
                    if self.point_values[assn] != total:
                        if assn == "008_pipelines":
                            grades[user] = cur / 5.0 * self.point_values[assn]
                        else:
                            grades[user] = cur / total * self.point_values[assn]
            except:
                grades[user] = 0
        return grades

    def _get_all_attendance(self):
        valid_locations = [
        ]
        with open('/root/wtmp', 'rb') as fd:
            entries = utmp.read(fd.read())
        for entry in entries:
            start_date = datetime.fromtimestamp(entry.sec)
            if (entry.user in users and
                start_date.hour in [9, 10, 11] and
                start_date.day in self.days and
                start_date.month in self.months):

                if any(l in entry.host for l in valid_locations):
                    if entry.user not in self.attendance:
                        self.attendance[entry.user] = {}
                    self.attendance[entry.user][start_date.day] = True
                # else:
                    # examined all these, they are all res ip hits
                    # print("MANUAL LINE: {} + {}".format(entry, start_date))

    def avg_assn(self, assn):
        return sum(
            [self.assignments[assn][user]
             for user
             in self.users]) / float(len(self.assignments[assn]))

    def user_total(self, user, prefix=None):
        return sum([self.assignments[assn][user]
                    for assn in self.assignments
                    if assn.startswith(prefix) or prefix is None])

    def get_class_totals(self):
        totals = {}
        for user in self.users:
            totals[user] = {}
            totals[user]['assns'] = self.user_total(user, prefix="0")
            totals[user]['midterm'] = self.user_total(user, prefix="mid")
            totals[user]['final'] = self.user_total(user, prefix="fnl")
        return totals


def get_users():
    users = []
    for (dirpath, dirnames,
         filenames) in os.walk(os.path.join(T4CS_ROOT,
                                            "logs/assns/state/")):
        for d in dirnames:
            if d.startswith("t4cs") or d in EXCLUDED_USERS:
                continue
            users.append(d)
        break
    return users


def get_config():
    path = os.path.join(T4CS_ROOT, PATH_TO_CONFIG)
    with open(path, 'r') as f:
        config = json.load(f)
    config['all_assignments'] = set(config['active_assignments'] +
                                    config['disabled_assignments'] +
                                    config['inactive_assignments'])
    return config


if __name__ == "__main__":
    jsonconfig = get_config()
    users = get_users()
    point_overrides = {
    }
    days = [3, 5, 10, 12, 17, 19, 26, 31]  # 24
    grades = Grades(assignment_list=jsonconfig['all_assignments'],
                    users=users,
                    point_values=point_overrides,
                    months=[7],
                    days=days)
    totals = grades.get_class_totals()
    # for assn in sorted(jsonconfig['all_assignments']):
    #     print("{} : {:.2} / {}".format(assn,
    #                                    grades.avg_assn(assn),
    #                                    grades.point_values.get(assn, 0)))
    for user in sorted(totals.keys()):
        print("{:10}a: {:0.1f}\tm: {}\tf: {}".format(
            user,
            totals[user]['assns'],
            totals[user]['midterm'],
            totals[user]['final']))

    possible = {
        'assns':
        sum([grades.point_values[k] for k in grades.point_values if k.startswith("0")]),
        'midterm':
        sum([grades.point_values[k] for k in grades.point_values if k.startswith("mid")]),
        'final':
        sum([grades.point_values[k] for k in grades.point_values if k.startswith("fnl")])
        }
    print("{:10}a: {}\tm: {}\tf: {}".format(
        "TOTALS",
        possible['assns'],
        possible['midterm'],
        possible['final'],

    ))

    #  Scale midterm grades
    mid_max = max([totals[u]['midterm'] for u in users])
    for user in users:
        totals[user]['midterm'] *= (possible['midterm'] / mid_max)

    #  Scale final grades

    fnl_max = max([totals[u]['final'] for u in users])
    for user in users:
        totals[user]['final'] *= (possible['final'] / fnl_max)

    for user in users:
        s_attend = sum([1 for d in grades.attendance[user]
                        if grades.attendance[user][d]]) / len(days)
        s_hw = totals[user]['assns'] / possible['assns']
        s_mid = totals[user]['midterm'] / possible['midterm']
        s_fnl = totals[user]['final'] / possible['final']
        s_tests = (0.33 * s_mid) + (0.33 * s_fnl) + (0.33 * max(s_mid, s_fnl))
        #s_tests = (0.50 * s_mid) + (0.50 * s_fnl)
        s_total = (s_attend * 0.5) + (s_hw * 0.2) + s_tests * (0.3)
        print(("{:10}A: {:>5.1f}\tH: {:>5.1f}\tM: {:>5.1f}"
               "\tF: {:>5.1f}\tT: {:>5.1f}"
               ).format(
                   user,
                   s_attend * 100,
                   s_hw * 100,
                   s_mid * 100,
                   s_fnl * 100,
                   s_total * 100))
