import json
import fcntl
import os

from time import time


class StateException(Exception):
    pass


class State(object):
    _state_fname = None
    _dirty = False
    _state = None

    def __init__(self, filename):
        self._state_fname = filename
        if not self._load_state():
            raise StateException("Couldn't load file")

    def is_initalized(self):
        # TODO -- egad why. That's not how we spell initialized...
        return self._state.get('initalized', False)

    def set_initialized(self, set_to):
        prev = self.is_initalized()
        # TODO -- egad why. That's not how we spell initialized...
        self.set('initalized', set_to)
        return prev

    def is_completed(self):
        return self._state.get('completed', False)

    def set_completed(self, set_to):
        prev = self.is_completed()
        self.set('completed', set_to)
        return prev

    def set(self, key, value, force=False):
        if not force and '.' in key:
            raise StateException("Keys must not contain dots.")

        self._state[key] = value

        self._dirty = True

    def get(self, key, default=None):
        return self._state.get(key, default)

    def unset(self, key):
        del self._state[key]

    def log_event(self, event, keep_for=None, force=False):
        if not force and '.' in event:
            raise StateException("Keys must not contain dots.")

        # We pull it out this way to set the list when it wasn't preexisting.
        lst = self._state.get(event, [])
        now = time()

        if keep_for:
            lst = [x for x in lst if x >= now - keep_for]

        lst.append(now)
        self._state[event] = lst

    def n_events_in_last(
            self, event, timeout, keep_for=None, force=False, set_now=False):
        """
        Number of times we've logged this event in the last 'timeout' seconds.

         - event: event name
         - timeout: time, in seconds, to check against
         - keep_for: number of seconds to keep history for
                     (useful for checking multiple thresholds on one event)
         - force: force this event as acceptable, even if it has invalid chars.
         - set_now: add an event to the history.

        Assumes keep_for >= timeout.
        """

        if not force and '.' in event:
            raise StateException("Keys must not contain dots.")
        lst = self._state.get(event, [])
        now = time()

        if set_now:
            lst.append(now)

        i = 0
        n_to_drop = 0
        for i, t in enumerate(lst):
            if keep_for and t < now - keep_for:
                n_to_drop += 1
            if t >= now - timeout:
                break

        n_new = len(lst) - i

        if keep_for:
            lst = lst[n_to_drop:]

        self._state[event] = lst
        return n_new

    # returns if everything is OK or not
    def _load_state(self):
        filename = self._state_fname
        if not os.path.isfile(filename):
            self._state = {}
        else:
            fstate = None
            try:
                fstate = open(filename, 'r+')
            except (IOError, OSError):
                # This is fatal
                return False
            try:
                fcntl.lockf(fstate, fcntl.LOCK_EX)
                self._state = json.load(fstate)
                fcntl.lockf(fstate, fcntl.LOCK_UN)
            except ValueError:
                # default case, its fine
                self._state = {}
            fstate.close()
        return True

    # returns if everything went OK
    def save_and_close(self):
        if not self._dirty:
            return True

        try:
            fstate = open(self._state_fname, 'w+')
            fcntl.lockf(fstate, fcntl.LOCK_EX)
            fstate.write(json.dumps(self._state))
            fcntl.lockf(fstate, fcntl.LOCK_UN)
            fstate.close()
        except IOError as e:
            return False

        return True
