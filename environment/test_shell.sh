#!/bin/bash

usage() {
    if [[ "$@" ]]; then
        echo -e "ERROR: $@\n" >&2
    fi

    echo -e "Spin up a test shell for T4CS's assignment checking.\n" >&2

    echo "Usage: $0 T4CS_ROOT" >&2
    exit 1
}

NUM_ARGS=1

if [[ "$#" -gt $NUM_ARGS ]]; then
    usage "Invalid Arguments"
fi

script_root=$(dirname $(readlink -f $0))/..
root=${1:-$script_root}

export T4CS_ROOT=$(readlink -f $root)
export T4CS_TEST=1

if [[ ! -e ${T4CS_ROOT}/logs/$USER ]]; then
    mkdir ${T4CS_ROOT}/logs/$USER
fi

bash --rcfile $T4CS_ROOT/environment/adduser/skel/.bashrc
