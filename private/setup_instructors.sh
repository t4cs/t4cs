#!/bin/bash

set -e

INSTRUCTORS_FILE=instructors
for INSTRUCTOR in $(cat ${INSTRUCTORS_FILE}); do
    password=$(mktemp -u XXXXXXXX)
    adduser \
        --quiet \
        --disabled-login \
        --gecos '' \
        $INSTRUCTOR
    chpasswd <<< $INSTRUCTOR:$password
    echo "$INSTRUCTOR pw: $password"
done
