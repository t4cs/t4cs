from __future__ import print_function

import os
import AssnUtils as utils


def get_repo(location):
    # If we can bail out early, we avoid importing git
    if not os.path.exists(location):
        return None

    import git
    try:
        return git.Repo(location)
    except:  # noqa
        return None


def get_bad_repo_err_msg(name):
    return ("The autograder can't find your repository " + name +
            " anymore! We won't be able to grade anything until you"
            " get back to the state it was in. (Try re-cloning it)")


def get_existing_local_repo(name, asn):
    r = get_repo(os.path.join(utils.get_user_assndir(asn), name))
    if r is None:
        errormsg = get_bad_repo_err_msg(name)
        if asn.state.get('last_announce') != errormsg:
            asn.error(errormsg)

    return r


def announce_on_track_if_needed(asn, name):
    errormsg = get_bad_repo_err_msg(name)
    if asn.state.get('last_announce') == errormsg:
        asn.announce_gated(("The autograder found repository {} again"
                            ". You can continue working on the task.").format(
                                name))


def get_recent_commit(r, idx=1):
    # TODO go back more than 1
    import git
    try:
        recent_commit = next(r.iter_commits('master', max_count=1))
    except git.exc.GitCommandError:
        return None
    return recent_commit


def get_current_commit(r):
    return r.head.commit


def commit_has_file(c, fname):
    if fname in list(c.stats.files.keys()):
        return True
    return False


def commit_has_added_file(c, fname):

    fstat = c.stats.files.get(fname, None)
    if fstat is None:
        return False
    # TODO Not a good test really...
    if (fstat.get('insertions') == fstat.get('lines') and
       fstat.get('deletions') == 0):
        return True

    return False


def commit_has_msg_string(c, string):
    if string in c.message or string.lower() in c.message:
        return True
    return False


def get_hidden_repo(name, asn):
    return get_repo(os.path.join(utils.get_user_hidden_assndatadir(asn),
                                 name))
