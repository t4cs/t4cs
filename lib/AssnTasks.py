"""Helper functions for running multi-task based assignments."""

import logging
import os.path

from collections import OrderedDict

import AssnUtils as utils


_logger = logging.getLogger("task")


QUESTION_INSTRUCTIONS = """
Answer questions with '{BINARY} answer {assignment} QUESTION_NAME ANSWER'
    (where QUESTION_NAME is the question name, e.g. {task})
"""

SUBMIT_INSTRUCTIONS = """
Submit scripts with '{BINARY} submit {assignment} TASK_NAME FILENAME'
    (where TASK_NAME is the task name, e.g. {task})
"""

HIDDEN_TASK_NOTE = """
({} blocked task{} not shown. Call with '-a' to show all.)
"""


class Assignment(utils.Assignment):
    # TODO - it'd be nice if we could have a task class, probably
    tasks = {}

    # High-level description of the assignment
    description = None

    def __init__(self, *args, **kwargs):
        # Order the tasks by their dependency graph, if they don't already
        # have an order.
        if type(self.tasks) != OrderedDict:
            self.tasks = OrderedDict(
                sorted(self.tasks.items(),
                       key=lambda t: get_sortkey(self.tasks, t[0])))

        super().__init__(*args, **kwargs)

    def is_task_complete(self, task_id):
        return self.state.get('DONE_' + task_id)

    def is_task_blocked(self, task):
        '''Unfortunately, takes the whole task dict, not just the name.'''
        blocked_by = []
        for task_id in task.get('dependencies', []):
            if not self.is_task_complete(task_id):
                blocked_by.append(task_id)
        return blocked_by

    def _mark_task_done(self, task_id):
        self.state.set('DONE_' + task_id, True)

    def _get_status_block(self, task_id, task, inc_desc=True):

        typ = 'question' if 'answer' in task else 'task'

        depends = ''
        blocked_by = self.is_task_blocked(task)
        if self.is_task_complete(task_id):
            status = utils.colored("COMPLETE", "green")
        elif blocked_by:
            status = utils.colored("BLOCKED", "yellow")
            depends = utils.colored(
                " (blocking: {})".format(
                    ", ".join(task['dependencies'])),
                "red")
        else:
            status = utils.colored("INCOMPLETE", "red")

        pts_i = task.get('points', 1)
        if pts_i == 1:
            pts = "({} pt)".format(str(pts_i))
        else:
            pts = "({} pts)".format(str(pts_i))

        desc = ""
        if inc_desc:
            description = utils.call_with_asn_or_string(
                task['description'], self)
            desc = "\n" + utils.nice_indent(
                description.strip(), width=78)

        returned = ('[{stat:^19}] {pts:7} {typ:26}' +
                    ' {tid:35}{depends:>49}').format(
            stat=status,
            typ=utils.colored("{}:".format(typ.capitalize()),
                              "cyan", attrs=["bold", "underline"]),
            pts=pts,
            tid=utils.colored(task_id, 'yellow', attrs=['bold']),
            depends=depends).rstrip() + "\n"
        if desc:
            returned += desc + "\n"
        return returned

    def next_msg(self, short=False):
        prefix = ''
        if self.description:
            prefix += "{}\n\n".format(self.description.strip())

        for task_id in self.tasks:
            task = self.tasks[task_id]

            complete = self.is_task_complete(task_id)
            blocked_by = self.is_task_blocked(task)
            if blocked_by or complete:
                continue

            suffix = ""
            if 'answer' in task and not short:
                suffix = "\n" + utils.colored(
                    QUESTION_INSTRUCTIONS.lstrip().format(
                        assignment=self.name, task=task_id,
                        BINARY=utils.EXE_NAME),
                    "yellow")
            if 'submit' in task and not short:
                suffix += "\n" + utils.colored(
                    SUBMIT_INSTRUCTIONS.lstrip().format(
                        assignment=self.name, task=task_id,
                        BINARY=utils.EXE_NAME),
                    "yellow")

            return (
                prefix +
                self._get_status_block(task_id, task, not short) +
                suffix)

        return None

    def info_msg(self, show_blocked_tasks=False, short=False):
        returned = ''
        if self.description:
            returned += "{}\n".format(self.description.strip())

        q_name = False
        s_name = False
        n_blocked = 0

        if short:
            returned += "\n"

        # Tasks
        for task_id, task in self.tasks.items():
            if not show_blocked_tasks and self.is_task_blocked(task):
                n_blocked += 1
                continue

            if not short:
                returned += "\n"

            returned += self._get_status_block(task_id, task, not short)

            if self.is_task_complete(task_id):
                continue

            if 'answer' in task:
                q_name = task_id

            if 'submit' in task:
                s_name = task_id

        if n_blocked:
            returned += utils.colored(HIDDEN_TASK_NOTE.format(
                n_blocked, 's' if n_blocked > 1 else ''), "magenta")

        if q_name:
            returned += "\n" + utils.colored(
                QUESTION_INSTRUCTIONS.lstrip().format(
                    assignment=self.name, task=q_name,
                    BINARY=utils.EXE_NAME),
                "yellow")
        if s_name:
            returned += "\n" + utils.colored(
                SUBMIT_INSTRUCTIONS.lstrip().format(
                    assignment=self.name, task=s_name,
                    BINARY=utils.EXE_NAME),
                "yellow")

        return returned

    def print_next_step(self):
        next_step = self.next_msg()
        if next_step:
            print("\n" + utils.nice_indent(next_step))

    def check_all_tasks_complete(self):
        if self.is_complete():
            return

        tasks_done, tasks_total = self.get_current_completion()
        if tasks_done == tasks_total:
            self.mark_complete()

        score, score_possible = self.get_current_score()
        self.assign_grade(score, score_possible)

    def handle_answer(self, qid, answer):
        surge_window = utils.check_surge_windows(self, "answer")
        if surge_window:
            self.error(
                "It seems like you're just guessing."
                " Please wait at least {} seconds before resuming.".format(
                    surge_window))
            return

        if qid not in self.tasks:
            self.error(
                "Question {} doesn't exist for this assignment.".format(qid))
            return

        task = self.tasks[qid]
        if "answer" not in task:
            self.error("Task {} is not a question.".format(qid))
            return

        result = task['answer'](self, answer)

        # If result is a tuple, then it's a (complete?, announce?) tuple.
        # Otherwise, we're implicitly just (result, True)
        complete, disp_announce = result, True
        if type(result) == tuple:
            complete, disp_announce = result

        if complete:
            if disp_announce:
                self.announce("Question {} answered correctly!".format(qid))

            self._advance_task(qid)

        elif disp_announce:
            self.error(
                "That's not the expected answer for question {}.".format(qid))

    def _advance_task(self, task_id):
        """Marks task done, prints next, and checks if all tasks done."""
        self._mark_task_done(task_id)
        self.print_next_step()
        self.check_all_tasks_complete()

    def handle_submit(self, task_id, filename):
        task = self.tasks.get(task_id, None)

        if not task:
            self.error("Invalid task {}.".format(task_id))
            return

        if 'submit' not in task:
            self.error("Task {} doesn't accept submissions.".format(task_id))
            return

        if not os.path.exists(filename):
            self.error("Submission file {} doesn't exist.".format(filename))
            return

        self._backup_submission(task_id, filename)

        if not task['submit'](self, filename):
            # It's the checker's problem to handle reporting a message.
            return

        self.announce("{} passed all tests!".format(filename))
        self._advance_task(task_id)

    def should_force_check(self, task_id):
        "Whether we should force checking on this task_id."
        task = self.tasks.get(task_id, None)
        if not task:
            return False
        return task.get("force_check", False)

    def handle_check(self):
        if self.is_complete():
            return

        for task_id in self.tasks:
            task = self.tasks[task_id]

            if (self.is_task_complete(task_id) and
                    not self.should_force_check(task_id)):
                continue

            if "checker" not in task:
                continue

            if self.is_task_blocked(task):
                continue

            if task['checker'](self):
                self.announce("Task \"{}\" complete!".format(task_id))
                self._advance_task(task_id)

        # In case something fell through the cracks, double-check completion.
        self.check_all_tasks_complete()

    def _backup_submission(self, task_id, filename):
        import datetime
        import random
        import shutil
        import string  # pylint: disable=W0402

        now = datetime.datetime.now()
        ts = now.strftime("%y%m%d-%H%M%S")
        base = os.path.basename(filename)
        username = utils.get_user()
        salt = "".join(
            [random.choice(string.ascii_letters) for x in range(16)])

        dfn = "{assn}-{task_id}-{user}-{ts}-{salt}-{basename}".format(
            task_id=task_id, user=username, ts=ts,
            salt=salt, basename=base, assn=self.name)

        destination = os.path.join(utils.LOGS_BASE_PATH, username,
                                   "assns/submissions", dfn)
        shutil.copyfile(filename, destination)
        _logger.info("Logged submission for %s to %s", task_id, dfn)

    def first_run(self):
        """Called once at the first opportunity after assignment is active."""
        self.announce("Assignment active.")
        print()
        print(utils.nice_indent(self.info_msg()))
        self.state.set_initialized(True)

    def print_status(self, print_style, print_short=False, plain=False):

        # Used for completion
        if plain:
            for task_name, task in self.tasks.items():
                if self.is_task_blocked(task):
                    continue

                print(task_name)
            return

        print_last = True
        if print_style == "info":
            msg = self.info_msg(False, print_short)
        elif print_style == "all":
            msg = self.info_msg(True, print_short)
        elif print_style == "next":
            msg = self.next_msg(print_short)
            print_last = False
        else:
            _logger.error("Unknown print_style: %s", print_style)
            return

        # TODO - Perhaps it should get its own default message?
        # TODO - or TAKE IN an assignment instead?
        #
        # TODO - everything about this function makes me miserable
        print(
            utils.colored("\n[{} Assignment]".format(utils.COURSE_NAME),
                          'magenta', attrs=['bold', 'underline']),
            utils.colored(self.name, 'yellow', attrs=['bold']))
        if self.is_complete():
            print(utils.colored(
                "Assignment " + self.name + " is complete!\n", 'green'))
        else:
            print("")
            # TODO -- this adds an extra line.
            print(utils.nice_indent(msg))
            last_announce = self.state.get('last_announce')
            if (print_last and
                    last_announce is not None and
                    not last_announce == msg):
                print(utils.nice_indent(utils.colored(
                    "Last message:", 'magenta', attrs=["bold"])))
                print(utils.nice_indent(
                    utils.colored(last_announce, 'cyan'), mult=2))

    def get_current_score(self):
        current = 0
        possible = 0
        for task_id in self.tasks:
            pts = int(self.tasks[task_id].get('points', 1))
            possible += pts
            if self.is_task_complete(task_id):
                current += pts

        return current, possible

    def get_current_completion(self):
        n_complete = len(self.tasks)
        if not self.is_complete():
            n_complete = 0
            for task_id in self.tasks:
                if self.is_task_complete(task_id):
                    n_complete += 1

        return n_complete, len(self.tasks)

    def get_status_line(self, is_active=False):
        tasks_done, tasks_total = self.get_current_completion()
        points_received, points_total = self.get_current_score()

        status = " ({:2} / {:2} tasks; {:2} / {:2} points)".format(
            tasks_done, tasks_total, points_received, points_total)

        # This run-around is so that the spacing works out with the colors.
        status = utils.colored(
            "{:>12}".format(status),
            "green" if tasks_done == tasks_total else (
                "red" if tasks_done == 0 else "yellow")
        )

        short_desc = (
            self.description.strip().split("\n", 1)[0][:60]
            if self.description else "")

        name_pad = "{:24}".format(self.name)
        name = utils.colored(name_pad, "green") if is_active else name_pad
        return "{} {:->65}\n    {}".format(
            name, status, short_desc)

    def get_task_names(self):
        return [self.tasks.keys()]


def get_assignment_from_module(assignment_module):
    name = assignment_module.__name__.rsplit(".", 1)[-1]
    assignment = assignment_module.Assignment(name=name)

    # TODO
    supplemental = getattr(assignment_module, 'SUPPLEMENTAL', None)

    # TODO
    init_setup = getattr(assignment_module, '_init_setup', None)
    if init_setup or supplemental:
        import sys
        sys.stderr.write("_init_setup or supplemental present\n")
        sys.exit(1)

    return assignment


# This is used to generate and traverse the dependency tree for tasks
# With a lambda, it's passed as key= to sorted()
def get_sortkey(tasks, task_id):

    class CircularDependencyException(Exception):
        pass

    task = tasks[task_id]

    if "sortkey" in task:
        return task["sortkey"]

    if not task.get('dependencies', None):
        task['sortkey'] = 0
        return 0

    # Protect against circular dependencies by locking the task.
    # There's really only risk of anything bad happening at the recursive step,
    # so we wait until here to lock, even though if we end up above during
    # sortkeying, we've still got a bug...
    if "sortkeying_in_progress" in task:
        return -1
    task['sortkeying_in_progress'] = True

    # Recurse
    dependencies = [get_sortkey(tasks, d) for d in task["dependencies"]]

    if min(dependencies) == -1:
        raise CircularDependencyException()

    task['sortkey'] = 1 + max(dependencies)

    del task['sortkeying_in_progress']
    return task['sortkey']
