#!/usr/bin/python3

import os
import sys
import json
import argparse
import termcolor

T4CS_ROOT = os.environ.get("T4CS_ROOT", "/var/t4cs")
sys.path.append(os.path.join(T4CS_ROOT, "lib"))

import AssnState  # noqa

banneduser = [
]

PATH_TO_CONFIG = "autograder/config.json"


def get_users():
    users = []
    for (dirpath, dirnames,
         filenames) in os.walk(os.path.join(T4CS_ROOT, "logs/")):
        for d in dirnames:
            if d.startswith("t4cs") or d in banneduser:
                continue
            users.append(d)
        break
    return users


def color_done(string):
    return termcolor.colored(string, 'green')


def color_incomplete(string):
    return termcolor.colored(string, 'yellow', attrs=['bold'])


def color_nostate(string):
    return termcolor.colored(string, 'red', attrs=['bold'])


def print_line(key, cur, total, config):
    if cur == -1:
        line = " {} : Not started".format(key)
        if config.text:
            print(line)
        else:
            print(color_nostate(line))
        return

    line = " {} : {} / {}".format(key, cur, total)

    if config.text:
        print(line)
    elif cur == total:
        print(" " + color_done(key))
    else:
        print(color_incomplete(line))


def print_user_states(user, config):
    print("=" * len(user))
    if config.text:
        print(termcolor.colored(user, "cyan"))
    else:
        print(termcolor.colored(user, "cyan"))
    print("=" * len(user))

    for assn in config.assns:
        grade_fname = os.path.join(T4CS_ROOT, "logs/", user,
                                   "assns/grades/", assn)
        cur = -1
        total = -1
        try:
            with open(grade_fname, 'r') as f:
                gr = json.load(f)
                cur = gr.get('score', 0)
                total = gr.get('possible', 0)
        except:  # noqa
            cur = -1
            total = -1

        print_line(assn, cur, total, config)


def print_assignment_states(assn, config):
    print("=" * len(assn))
    print(termcolor.colored(assn, "cyan"))
    print("=" * len(assn))
    total_done = 0
    total_missed = 0
    for user in config.users:
        state_fname = os.path.join(T4CS_ROOT, "logs", user,
                                   "assns/state/", assn)
        grade_fname = os.path.join(T4CS_ROOT, "logs", user,
                                   "assns/grades/", assn)
        try:
            AssnState.State(state_fname)
        except AssnState.StateException:
            print(color_nostate(user))
            total_missed += 1
            continue
        cur = 0
        total = 0
        try:
            with open(grade_fname, 'r') as f:
                gr = json.load(f)
                cur = gr.get('score', 0)
                total = gr.get('possible', 0)
                if cur == total:
                    total_done += 1
        except:  # noqa
            total_missed += 1
            cur = -1
            total = -1
        print_line(user, cur, total, config)

    if not config.quiet:
        print("{} / {} users competed assignment."
              .format(total_done, len(config.users)))
        print("{} / {} users have state."
              .format(len(config.users) - total_missed, len(config.users)))


def get_config():
    path = os.path.join(T4CS_ROOT, PATH_TO_CONFIG)
    with open(path, 'r') as f:
        config = json.load(f)
    config['all_assignments'] = (config['active_assignments'] +
                                 config['disabled_assignments'] +
                                 config['inactive_assignments'])
    return config


def get_args(jsonconfig):
    parser = argparse.ArgumentParser(
        description='assignment status checking,'
        ' must specify at least one option',
        prog="assignment_status")

    parser.add_argument('-u', '--users', nargs='+',
                        default=None,
                        help='users to see all status for')
    parser.add_argument('-a', '--assns', nargs='+',
                        default=None,
                        help='assns to see all users for')

    parser.add_argument('-q', '--quiet', action="store_true",
                        default=False,
                        help='Suppress summary of assignment printing')
    parser.add_argument('-t', '--text', action="store_true",
                        default=False,
                        help='Text only output, no colors')

    args = parser.parse_args()
    if args.users is None and args.assns is None:
        parser.print_help()
        exit(-1)

    action = 'users'
    if args.users is None:
        args.users = sorted(get_users())
        action = 'assns'
    if args.assns is None:
        args.assns = sorted(jsonconfig['all_assignments'])
        action = 'users'
    return args, action


if __name__ == "__main__":
    jsonconfig = get_config()
    config, action = get_args(jsonconfig)
    print("{} {} {}".format(color_nostate("nostate"),
                            color_incomplete("incomplete"),
                            color_done("done")))

    # Fix up any incomplete assn names
    config.assns = [rassn for a in config.assns for rassn in
                    jsonconfig['all_assignments'] if rassn.startswith(a)]

    if action == 'users':
        for user in config.users:
            print_user_states(user, config)
    elif action == 'assns':
        for assn in config.assns:
            print_assignment_states(assn, config)
