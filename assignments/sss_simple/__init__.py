
import os

# You will need both libraries for almost any assignment
import AssnUtils as utils
import AssnTasks

# As a general rule, we created the ASSIGNMENT_TASKS dictionary as a
#  static class object. Each entry is a discrete task the student
#  completes. This can be steps to complete an assignment, or
#  individual unreleated questions.
ASSIGNMENT_TASKS = {
    # We asked students to do all their work in an 'assignment
    #  directory' which was a directory named after the assignment in
    #  their home directory. This makes automatedly searching for
    #  files/parts of their work much simpler. We have several utils
    #  functions that handle figuring this out.

    # See the AssnTasks library to see the different task types
    # (checker, submit, answer)

    # This task is a _checker_ task, meaning as long as the assignment
    #  is active, the checker will run after _every single command_
    #  the student performs. These can be great because its fully
    #  automated from the student's perspective, but its important to
    #  be careful in how much work the checker does.
    "assn_dir_made": {
        "name": "make assignment directory",
        "description":
            "This assignment requires manipulating"
            " files. As a result, it's best to put everything in it's own"
            " directory. Create a directory named after this assignment in"
            " your home directory. All subsequent edits should be done in this"
            " directory",
        "checker": lambda asn: os.path.exists(utils.get_user_assndir(asn))
    },
}


# Now create the assignment with all of its subtasks. In more complex
#  assignments we might do some work here that we can't do in the
#  static object
class Assignment(AssnTasks.Assignment):
    # AssnTasks will handle managmeent of everything from here on.
    tasks = ASSIGNMENT_TASKS
